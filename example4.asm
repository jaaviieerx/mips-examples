.text
.globl main
main:
   
 #The following block of code is to pre-load the integer values representing the various instructions into registers for storage
    li $t3, 1    #This is to load the immediate value of 1 into the temporary register $t3
    li $t4, 5    #This is to load the immediate value of 2 into the temporary register $t4
    li $t5, 9   #This is to load the immediate value of 3 into the temporary register $t5

	add $t6, $t3, $t4	#This is to add T3 and t4 and send answer to t6

    add $t0, $t3, $t5
    add $t1, $t3, $t4
    sub $t7, $t0, $t1
    
 li $v0, 10			#This is to end the process
syscall
